import 'package:flutter/material.dart';

void main() {
  runApp(const firstapp());
}

class firstapp extends StatelessWidget {
  const firstapp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: Scaffold(
          appBar: AppBar(
            leading: Icon(Icons.ad_units_outlined),
            title: Text('My First App'),



            actions: [
              IconButton(onPressed: () {},
                  icon: Icon(Icons.ad_units)),
              IconButton(onPressed: () {},
                  icon: Icon(Icons.ad_units)),
            ],



          ),
          body: Center(
            child: Column(
              children: [
                // Image.asset('assets/jom.jpg',
                // height:300,
                // width: 300,),
                CircleAvatar(
                  backgroundImage: AssetImage('assets/jom.jpg'),
                  radius:125
                ),
                Text('Panitan Kuedoy',
                    style: TextStyle(
                        color: Colors.indigo,
                        fontWeight: FontWeight.bold,
                        fontSize: 40)),
                Text('6350110013',
                    style: TextStyle(
                        color: Colors.lightBlueAccent,
                        fontWeight: FontWeight.bold,
                        fontSize: 25)),
              ],
            ),
          )),
    );
  }
}
